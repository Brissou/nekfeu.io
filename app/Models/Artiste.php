<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Artiste extends Model
{
    protected $fillable = [
        'name',
        'descriptif',
        'style_id',
        'photo'
    ];

    public function style() {
        return $this->belongsTo(Style::class);
    }

    public function albums() {
        return $this->hasMany(Album::class);
    }

}
