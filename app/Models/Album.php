<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $fillable = [
        'name',
        'descriptif',
        'illustration'
    ];

    public function artiste() {
        return $this->belongsTo(Artiste::class);
    }

    public function morceaux() {
        return $this->hasMany(Morceau::class);
    }
}
