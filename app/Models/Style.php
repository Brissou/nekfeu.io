<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Style extends Model
{
    public $fillable = [
        'name',
        'descriptif'
    ];

    public function artistes() {
        return $this->hasMany(Artiste::class);
    }
}
