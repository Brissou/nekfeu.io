<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    protected $fillable = [
        'avatar',
        'name',
        'score',
        'answer'
];

    public function messages()
    {
    return $this->hasMany(Message::class);
    }
}
