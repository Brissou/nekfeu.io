<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Morceau extends Model
{
    protected $fillable = [
        'titre',
        'paroles',
        'mp3',
        'duree',
        'feat',
        'artiste_id',
        'album_id',
    ];

    public function album() {
        return $this->belongsTo(Album::class);
    }
}
