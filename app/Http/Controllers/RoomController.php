<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Player;
use App\Models\User;
use Auth;

class RoomController extends Controller
{

    public function setupRoom(Request $request) {

        $user = new User;
        $user->name = $request->name;
        $user->avatarcode = $request->avatar;
        $user->roomid = $request->roomid;
        $user->points = 0;

        if($request->host) {
            $user->host = 1;
        }

        $user->save();

        Auth::loginUsingId($user->id);
        return $user->roomid;
    }


       //Store dans la bdd
       public function index($roomid) {

        $current = Auth::user();

           return view('room')->with([
               'current' => $current,
                'name' => session('name'),
                'avatar' => session('avatar'),
                'roomid' => session('roomid')
           ]);
       }



}
