<?php

namespace App\Http\Controllers;

use Request; 

class WelcomeController extends Controller
{
    
    public function index() {
        return view('welcome')->with([
            'host'=> true
        ]);    
    }

    public function getInvited() {

        // Met en session roomid
        session(['roomid' =>Request::segment(2)]);

        return view('welcome')->with([
            'host' => false,
            'roomid' => Request::segment(2)
        ]);        
    }

}
