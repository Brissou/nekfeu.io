<?php

namespace App\Http\Controllers;
use App\Models\Album;
use App\Models\Artiste;
use App\Models\Style;
use App\Models\Morceau;

use Illuminate\Http\Request;

class GameController extends Controller
{
    public function index() {
        //atm only nekfeu
        $artiste = Artiste::first();
        return view('game')->with([
            'albums' => $artiste->albums
        ]);
    }
}
