<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMorceauxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('morceaux', function (Blueprint $table) {
            $table->id();
            $table->string('titre');
            $table->text('paroles')->nullable();
            $table->string('mp3')->nullable();
            $table->time('duree')->nullable();
            $table->string('feat')->nullable();

            $table->bigInteger('artiste_id')->unsigned();
            $table->foreign('artiste_id')->references('id')->on('artistes');
            $table->bigInteger('album_id')->unsigned();
            $table->foreign('album_id')->references('id')->on('albums');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('morceaux');
    }
}
