<?php

use Illuminate\Support\Facades\Route;
use App\Events\NewMessage;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();



//Première page
Route::get('/', 'WelcomeController@index');

// Vue qui renvoie une URL avec un avatar random
Route::get('/generate/randomavatar', function(){
    $url = BigHeads\Factory::create()->getUrl();
    $image = file_get_contents($url);
    if ($image !== false){
        return 'data:image/svg+xml;base64,'.base64_encode($image);
    }
});

//Deuxième page
Route::get('/room/{idroom}', 'RoomController@index')->name('room');
Route::post('/setuproom', 'RoomController@setupRoom')->name('setuproom');

// Troisième page
Route::get('/game/{idroom}', 'GameController@index')->name('game');

// Pour invite 
Route::get('/invite/{idroom}', 'WelcomeController@getInvited')->name('invite');


//chat
Route::get('/event/{idroom}', function() {
    event(new NewMessage($message));
});


// Route::get('/', 'ChatsController@index');
// Route::get('messages', 'ChatsController@fetchMessages');
// Route::post('messages', 'ChatsController@sendMessage');





