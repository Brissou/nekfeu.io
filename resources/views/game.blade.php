@extends('layouts.app')
@section('content')

    <div class="container game d-flex flex-column flex-grow-1">
        <div class="d-flex align-items-center justify-content-between mb-2">
            <div>
                <div class="d-flex align-items-center text-white">
                    <div class="flex-grow-1 text-center">
                        <img src="{{ session('avatar') }}" alt="Avatar" class="img-fluid avatar">
                        <h6 class="text-center mt-1">{{ session('name') }}</h6>
                    </div>
                    <h5 class="font-weight-bold">
                        12pts
                    </h5>
                </div>
            </div>
            <div class="align-self-start">
                <svg width="50%" height="50%" style="width:10vw; max-height:65px" viewbox="0 0 250 250">
                    <path id="border" transform="translate(125, 125)" />
                    <path id="loader" transform="translate(125, 125) scale(.84)" />
                </svg>
            </div>
            <div>
                <div class="d-flex flex-row-reverse align-items-center text-white">
                    <div class="flex-grow-1 text-center">
                        <img src="{{ session('avatar') }}" alt="Avatar" class="img-fluid avatar">
                        <h6 class="text-center mt-1">{{ session('name') }}</h6>
                    </div>
                    <h5 class="font-weight-bold">
                        12pts
                    </h5>
                </div>
            </div>
        </div>

        <div class="col text-white bg-game text-center pt-4 px-3 d-flex flex-column justify-content-around">
                <div class="theme-title text-center">
                    <h3 class="text-primary bg-light shadow-sm px-3">Les classiques</h3>
                </div>

                <div>
                    <h3 class="question-title text-uppercase">"Les bouches se collent et l'école se bouche [...]"</h3>
                </div>

                <h5 class="question-question mb-3 mt-2">
                    <span class="bg-light text-dark px-3 py-1 rounded shadow-sm">
                        De quel album provient ces lyrics ? 
                    </span>
                </h5>

                <div class="row justify-content-center align-self-center">
                    @foreach ($albums as $album)
                        <div class="col-6 mb-3 text-center">
                            <div class="d-flex align-items-center justify-content-center tilt-album rounded shadow-sm overlay2" style="background:url('{{ $album->photo }}');" data-tilt data-tilt-scale="1.1">
                                <h2 class="titre-album">{{$album->titre}}</h2>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-6 mb-3 text-center">
                        <div class="d-flex align-items-center justify-content-center tilt-album rounded shadow-sm overlay2" style="background:url('{{ $album->photo }}');" data-tilt data-tilt-scale="1.1">
                            <h2 class="titre-album">{{$album->titre}}</h2>
                        </div>
                    </div>
                </div>
        </div>



    </div>


@endsection
