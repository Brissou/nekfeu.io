@extends('layouts.app')
@section('content')
    @php
        $url = BigHeads\Factory::create()->getUrl();
        $image = file_get_contents($url);
        if ($image !== false){
            $bigHead = 'data:image/svg+xml;base64,'.base64_encode($image);
        }
        $uniqid = uniqid();
    @endphp
    <div class="container mb-5">
        <div class="d-flex flex-column h-100 align-items-center justify-content-center">
            {!! Form::open(['url' => 'setuproom', 'id' => 'setuproom', 'type' => 'POST', 'class' => 'text-center']) !!}
            <div class="avatar-section form-group">
                <img src="{{ $bigHead }}" alt="Avatar" class="avatar">
                <div class="btn rounded-circle shadow-sm reload-icon">
                    <i class="fas fa-sync-alt"></i>
                </div>
            </div>
            <div class="form-group">
                {!! Form::text('name', null, ['class' => 'form-control input-pseudo mt-4', 'placeholder' => 'Mon pseudo', 'maxlength' => '14']) !!}
            </div>
            {!! Form::submit(($host) ? 'Créer la partie' : 'Rejoindre la partie', ['class' => 'btn btn-light btn-lg shadow-sm font-weight-bold mt-3 px-3 submit text-primary']) !!}
            
            {!! Form::hidden('host', $host) !!}
            {!! Form::hidden('avatar', $bigHead) !!}
            @if($host)
            {!! Form::hidden('roomid', $uniqid) !!}
            <a href="{{route('room', $uniqid)}}" class="d-none next-page"></a>
            @else
            {!! Form::hidden('roomid', last(request()->segments())) !!}
            <a href="{{route('room', last(request()->segments()))}}" class="d-none next-page"></a>
            
            @endif
            {!! Form::close() !!}

        </div>
    </div>

@endsection
