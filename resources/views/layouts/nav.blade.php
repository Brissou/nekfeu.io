<nav class="navbar">
    <div class="container">
          <button class="hamburger hamburger--elastic" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"  aria-label="{{ __('Toggle navigation') }}">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
            </span>
          </button> 
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{asset('image/logo.png')}}" class="logo" alt="">
        </a>
        <div class="text-white info">
            <i class="fas fa-info"></i>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>
            
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                {{-- <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"  v-pre>
                            {{ Auth::user()->name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest --}}
            </ul>
        </div>
    </div>
</nav>

<script>

$(function() {
    $('.hamburger').on('click', function(){
        $(this).toggleClass('is-active');
    })


    $('.info').on('click', function() {
        swal.fire({
            title: 'INFORMATIONS SUPP.',
            // icon: 'info',
            html:
            `
            <div class="text-left">
            <h3>Concept</h3>
            <p>C'est un mélange de Blind-test, de connaissances sur les lyrics et de n'"oubliez pas les paroles" sur Nekfeu.</p>
            <p>Invite un ami et découvre par toi même 🔥</p>
            <hr>
            <h3>Contact</h3>
            <p>Salut mon kho, j'espère que tu apprécies ce petit jeu que j'ai développé avec l'aide de mon ami Lucas.</p>
            <p>Je travaille en ce moment sur une version bien plus poussée qui ne concerne pas que Nekfeu..</p>
            </div>
            <div class="alert alert-danger text-center">
                Si jamais tu vois un bug, une erreur ou que tu me trouves séduisant, hésite pas à me le faire remonter en mp sur Insta
            </div>
            <div class="d-flex justify-content-around">
                <a href="https://www.instagram.com/lloyd_yx/" target="_blank" class="btn-insta text-decoration-none">
                    <i class="fab fa-instagram"></i>
                    <span>Lucas</span>
                </a>
                <a href="https://www.instagram.com/brice_eliasse/" target="_blank" class="btn-insta text-decoration-none">
                    <i class="fab fa-instagram"></i>
                    <span>Brice</span>
                </a>
            </div>
            `

            ,
            showCloseButton: true

        })
    });
})
</script>