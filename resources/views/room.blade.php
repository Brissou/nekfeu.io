@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-5">
                <div class="avatar-section">
                    <img src="{{$current->avatarcode}}" alt="Avatar" class="img-fluid avatar player1-avatar">
                    <h3 class="text-center text-light player1-name">{{$current->name}}</h3>
                </div>
            </div>
            <div class="col-2 d-flex align-items-center p-0">
                <div class="vs"></div>
            </div>
            <div class="col-5 d-flex align-items-end justify-content-center colp2">
                <div class="avatar-section">
                    <img src="{{asset('image/pending.jpg')}}" alt="Avatar" class="img-fluid avatar player2-avatar img-pending">
                    <h3 class="text-center text-light player2-name">...</h3>
                </div>
            </div>
            @if($current->host)
                <div class="col-12 mt-5 text-center">
                    {!! Form::text('room', url('/invite/'.$current->roomid), ['class' => 'hiddenurl', 'id' => 'idroom'], ['readonly']) !!}
                    <button class="btn btn-lg btn-light shadow-sm font-weight-bold mt-3 px-3 submit text-primary" id="copybtn"
                    ><i class="ml-1 far fa-copy mx-1"></i> Copier</button>
                    <a href="{{ route('game', $current->roomid) }}" 
                        class="btn btn-light btn-lg shadow-sm font-weight-bold mt-3 px-3 submit text-primary disabled"><i
                        class="fab fa-google-play mx-1"></i> Démarrer</a>
                </div>
                @endif
        </div>
    </div>

    <div class="container-fluid chat">
        <div class="card">
            <div class="card-header">
                <div class="row align-items-center">
                    <div class="col-2">
                        <img src="{{ $current->avatarcode }}" class="img-fluid" alt="">
                    </div>
                    <div class="col-10">
                        <h5 class="m-0">Chat avec Lucas</h5>
                    </div>
                </div>
            </div>
            <div class="card-body text-white conv">
                Vous n'avez pas encore écrit..
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-10">
                        {!! Form::text('message', null, [
                        'placeholder' => 'Écrit ton message...',
                        'class' => 'input-msg
                        form-control',
                        ]) !!}
                    </div>
                    <div class="col-2 text-center">
                        <button type="button" class="btn btn-light w-100 send-msg">
                            <i class="fas fa-paper-plane"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
