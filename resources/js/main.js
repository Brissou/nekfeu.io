document.addEventListener( 'DOMContentLoaded', function () {
	const swup = new Swup({
        plugins: [new SwupSlideTheme()]
    }); 


    function init() {

        if (document.querySelector('#welcome')) {
            // console.log('welcome !')
            $('.reload-icon').on('click', function() {
                $.ajax({
                    url: '/generate/randomavatar',
                    type: 'GET',
                    success: function(data) {
                        $('.avatar').attr('src', data);
                        $('[name ="avatar"]').attr('value', data);
                    },
                    error: function() {
                        console.log('Erreur generate random avatar ajax');
                    }
                });
            });
    
    
                // this is the id of the form
                $("#setuproom").submit(function(e) {
    
                e.preventDefault(); // avoid to execute the actual submit of the form.
    
                var form = $(this);
                var url = form.attr('action');
                $.ajax({
                    type: "POST",
                    url: url,
                    data: form.serialize(), // serializes the form's elements.
                    success: function(data)
                        {
                            localStorage.setItem("roomId", data);
                            $('.next-page')[0].click();
                        }
                    });
    
    
                });
    
                if ($('.form-control').val().length == 0) {
                    $('.submit').attr('disabled', true);
                }
                $('.form-control').keyup(function() {
                    if ($(this).val().length != 0)
                        $('.submit').attr('disabled', false);
                    else
                        $('.submit').attr('disabled', true);
                })
            }



	    if (document.querySelector('.input-msg')) {
            // console.log('input msg here ');

                 // Detect ENTER on chat (send + clear)
                var input = $('.input-msg');
                input.on("keyup", function(event) {
                if (event.keyCode === 13) {
                    event.preventDefault();
                    $('.send-msg').click();
                }
              });
                $('.send-msg').on('click', function(){
                    console.log(input.val());
                    input.val('');
                })
        
        }
        
        if (document.querySelector('#room')) {
            const roomId = localStorage.getItem("roomId");
            Echo.join(`chat.${roomId}`)
                .here((users) => {
                    if(users.length == 2){
                        $('.submit').removeClass('disabled');
                        $('.colp2').removeClass('d-flex align-items-end justify-content-center');
                        $('.player2-avatar').attr('src', users[0].avatarcode);
                        $('.player2-avatar').removeClass('img-pending');
                        $('.player2-name').text(users[0].name);
                        $('.submit').removeClass('disabled');
                    }
                    console.log(users);
                })
                .joining((user) => {

                    // Pour le joueur 1 quand le joueur 2 join
                    $('.colp2').removeClass('d-flex align-items-end justify-content-center');
                    $('.player2-avatar').attr('src', user.avatarcode);
                    $('.player2-avatar').removeClass('img-pending');
                    $('.player2-name').text(user.name);
                    $('.submit').removeClass('disabled');

                    console.log(user.name);
                })
                .leaving((user) => {
                    console.log(user.name +' s\'est barré');
                })
                .listen('NewMessage', (e) => {
                    console.log(e);
                })

                .error((error) => {
                    console.error(error);
                });

                Echo.channel(`messages.${roomId}`)
                .listen('NewMessage', (e) => {
                    console.log(e);
                });
            // console.log('room js loaded ');
              // Clipboard
            function copyToClipboard() {
                var copyText = document.getElementById("idroom");
                copyText.select();
                copyText.setSelectionRange(0, 99999); /*For mobile devices*/
                document.execCommand("copy");
                $('#copybtn').text('Copié !')
            }
            $('#copybtn').on('click', function() {
                copyToClipboard();
            })

        }
        
        if (document.querySelector('#game')) {
            console.log('game loaded');

            var loader = document.getElementById('loader'),
            border = document.getElementById('border'),
            α = 360,
            π = Math.PI,
            t = 30;

        (function draw() {
            α--;
            α = α === 0 ? 360 : α;

            var r = (α * π / 180),
                x = Math.sin(r) * 125,
                y = Math.cos(r) * -125,
                mid = (α > 180) ? 1 : 0,
                anim = 'M 0 0 v -125 A 125 125 1 ' +
                mid + ' 1 ' +
                x + ' ' +
                y + ' z';

            loader.setAttribute('d', anim);
            border.setAttribute('d', anim);

            setTimeout(draw, t); // Redraw
        })();


        $('.tilt-album').tilt();
        }
	}

	init();
	
	swup.on('contentReplaced', init);

    
} );


