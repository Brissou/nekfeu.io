require('./bootstrap');
require('tilt.js');
window.Swup = require('swup').default;
window.SwupSlideTheme = require('@swup/slide-theme');
require('./main');


const swal = window.swal = require('sweetalert2');

const io = require("socket.io-client");
window.io = io; 